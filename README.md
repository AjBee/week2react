# Homework EAD
## due date: 14/9/2018

## Clone and pull this project
1. `git clone https://gitlab.com/AjBee/week2react.git`
2. Run `npm install` on your project by command line
3. Create your component under /src/homework/  directory
4. Check an example from /src/homework/AjBee.jsx
5. Edit App.js to append your component to main page


## Example of /src/homework/AjBee.jsx
```javascript
import React, { Component } from "react";

class AjBee extends Component {
  render() {
    return (
      <div style={beestyle.container}>
          <h1 style={beestyle.header}>Hello AjBee</h1>
      </div>
    );
  }
}

const beestyle={
    container:{border: '1px solid black'},
    header:{color:"red"}

}

  
export default AjBee;


```

## Adding your component into App.js
```javascript
import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import AjBee from "./homework/AjBee";
import ___your component___ from "./homework/_____";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <AjBee/>
        <___________/>
        
      </div>
    );
  }
}

```