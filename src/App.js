import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import AjBee from "./homework/AjBee";
import JackPlus from "./homework/TheeranaiSangjan";
import PhattarawanBoonpeng from "./homework/PhattarawanBoonpeng";
import Yanisayawichai from "./homework/YanisaYawichai";
import Sapa058 from "./homework/Sapa058"
import NorawidNara from "./homework/NorawidNara";
import JarawiNuisai from "./homework/JarawiNuisai";
import ThanapatSanongyat from "./homework/ThanapatSanongyat";
import Muashawae from './homework/Muashawae';
import PisitIntamo from "./homework/PisitIntamo";
import NattaponDeebang from "./homework/NattaponDeebang";
import PapitchayaMukdaphirom from "./homework/PapitchayaMukdaphirom";
import WirapongDunghataipounsuk from "./homework/WirapongDunghataipounsuk";
import SiraphatP from "./homework/SiraphatP";
import Kanochai from "./homework/KanokchaiAmaphut";
import JirayutLaowpanich from "./homework/JirayutLaowpanich";
import SukdaPrompalit from "./homework/SukdaPrompalit";
import SutussaSanon from "./homework/SutussaSanon";
import SaranSrisathon from "./homework/SaranSrisathon";
import ThunchanokDuangarno from "./homework/ThunchanokDuangarno";
import KeeratiKaenchan from "./homework/KeeratiKaenchan";
import ThirawatPrasert from "./homework/ThirawatPrasert";
import KatechanokWisawanawin from "./homework/KatechanokWisawanawin";
import NapapatPattarayuttawatt from "./homework/NapapatPattarayuttawatt";
import Sirinda from "./homework/Sirinda";
import SinsupaSrethao from "./homework/SinsupaSrethao";
import ThanathatTonthong from "./homework/ThanathatTonthong";
import RatchanonYorngchana from "./homework/RatchanonYorngchana";
import NopparatYuyen from "./homework/NopparatYuyen";
import DussadeeLeesan from "./homework/DussadeeLeesan";
import KittapornChaicharoen from "./homework/KittapornChaicharoen";
import Chonlatee from "./homework/chonlatee";
import PennapaKhanthanan from "./homework/PennapaKhanthanan";
import KanokwanRuanthai from "./homework/KanokwanRuanthai";
import SareepapIntajai from "./homework/SareepapIntajai";
import SuvichanKumtue from "./homework/SuvichanKumtue";
import Sirasit from "./homework/SirasitUngpinitpong";
import NantakornNuchit from "./homework/NantakornNuchit";
import PiyapongUkoad from "./homework/PiyapongUkoad";
import Sasina from "./homework/SasinaJaiprong";
import Yotsaphon from "./homework/Yotsaphon";
import Siwadon from "./homework/SiwadonBoonpradub";
import Pannaja from "./homework/PanpanutHeabthong";
import Anthika from "./homework/AnthikaJirattananon";
import Kanyanut from "./homework/KanyanutJaihaow";
import Sariz from "./homework/SarizWachirasook";
import Sukrita from "./homework/Sukrita";
import Pornjarat from "./homework/PornjaratKahathum";
import Wasinee049 from "./homework/WasineeDokmai";


class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>

     <AjBee />
        <JackPlus />
        <Yanisayawichai />
        <PhattarawanBoonpeng />
        <NorawidNara />
        <JarawiNuisai />
        <ThanapatSanongyat />
        <PisitIntamo />
        <NattaponDeebang />
        <NantakornNuchit />
        <PapitchayaMukdaphirom />
        <WirapongDunghataipounsuk />
        <SiraphatP />
        <Kanochai />
        <JirayutLaowpanich />
        <SukdaPrompalit />
        <SutussaSanon />
        <Muashawae />
        <SaranSrisathon />
        <ThunchanokDuangarno />
        <KeeratiKaenchan />
        <ThirawatPrasert />
        <KatechanokWisawanawin />
        <NapapatPattarayuttawatt />
        <Sirinda />
        <SinsupaSrethao />
        <ThanathatTonthong />
        <RatchanonYorngchana />
        <NopparatYuyen />
        <DussadeeLeesan />
        <KittapornChaicharoen />
        <Chonlatee />
        <PennapaKhanthanan/>
        <KanokwanRuanthai/>
        <SuvichanKumtue/>
        <Sirasit/>
        <SareepapIntajai/>
        <PiyapongUkoad />
        <Sasina />
        <Yotsaphon />
        <Siwadon />
        <Pannaja />
        <Anthika />
        <Kanyanut />
        <Sariz />
        <Sukrita/>
        <Pornjarat/>
        <Sapa058 />
        <Wasinee049/>
      </div>
    );
  }
}

export default App;
