import React, { Component } from 'react';

class SareepapIntajai extends Component {
    render() {
        const imgLink = 'https://pre00.deviantart.net/465f/th/pre/i/2017/276/0/9/black_clover___asta_by_mssaltyanchor-dbpes1i.jpg';
        return (
            <div style={styles.contrinerprofilesty}>
                <table style={styles.tablesty}>
                    <tr>
                        <img style={styles.imgsty} src={imgLink} alt="Sareepap" />
                    </tr>
                    <tr>
                        <h1 style={styles.txth1sty}>5931305066</h1>
                    </tr>
                    <tr>
                        <h2 style={styles.txth2sty}>Sareepap Intajai</h2>
                    </tr>
                    <tr>
                        <h2 style={styles.txth2sty}>Software Engineering</h2>
                    </tr>
                </table>
            </div>
        );
    }
}

const styles = {
    contrinerprofilesty: {
        backgroundColor: 'rgb(245, 241, 183)',
        border: '8px solid rgb(60, 48, 87)',
        borderWidth: '2',
        margin: 'auto',
        padding: '10px',
        width: '350px',
        marginTop: '20px',
        marginBottom: '20px',
    },
    tablesty: {
        margin: 'auto',
        border: '8px',
        borderColor: 'red',
    },
    imgsty: {
        height: '180px',
        width: '180px',
    },
    txth1sty:{
        color:'DarkSlateBlue',
    },
    txth2sty: {
        color: 'Maroon',
    },
}

export default SareepapIntajai;
