import React, { Component } from 'react';
// import './App.css';

class Sirasit extends Component {
  render() {

    return (
      <div className="App">
        <div style={me.main}>
          <HeaderBar  myname="Sirasit Ungpinitpong"/>

          <CardTop  idstd="5931305055"
                    nickname="Sun"/>

          <ListDetail univer="Mae Fah Luang"
                      school="Information Technology"
                      major="Software Engineering"/>
        </div>
      </div>
    );
  }
}

class HeaderBar extends React.Component {
  render(){
    const profile = 'https://scontent.fbkk9-2.fna.fbcdn.net/v/t1.0-9/39956153_1910703418976642_4634170176516915200_o.jpg?_nc_cat=0&oh=e7f3d7f7f3663d7bdcc819c8a619c6c4&oe=5C38DEC4';
    return (
      <div style={me.frame}>
          <header style={me.head}>
              <div style={me.about}>
                <div style={me.profile}>
                  <img src={profile} alt="profile" style={me.profile} className="App-logo"/>
                </div>
                <div>
                  <h1 style={me.title}>About Me!</h1>
                  <h1 style={me.name}>{this.props.myname}</h1>
                </div>
              </div>
          </header>
        </div>
    );
  }
}

class CardTop extends React.Component {
  render(){
    return(
      // Card ID and Nickname
      <div style={me.row}>
      {/* Card 1 */}
      <div style={me.card}>
          <h3 style={me.textCard}>id</h3>
          <h1 style={me.name}>{this.props.idstd}</h1>
      </div>
      {/* Card 1 */}
      <div style={me.card}>
          <h3 style={me.textCard}>nickname</h3>
          <h1 style={me.name}>{this.props.nickname}</h1>
      </div>
    </div>
    );
  }
}

class ListDetail extends React.Component {
  render(){
    return(
      // List group detail
      <div style={me.group}>
      <div style={me.cardHead}>
        <h6 style={me.tHead}>Detail</h6>
      </div>
      <div style={me.cardBody}>
        <ul style={me.listGroup}>
          <li style={me.listItem}> 
            <span style={me.textLeft}>University</span>
            <span style={me.textRight}>{this.props.univer}</span>
          </li>
          <li style={me.listItem}> 
            <span style={me.textLeft}>School of</span>
            <span style={me.textRight}>{this.props.school}</span>
          </li>
          <li style={me.listItem}> 
            <span style={me.textLeft}>Major</span>
            <span style={me.textRight}>{this.props.major}</span>
          </li>
        </ul>
      </div>
      <div style={me.cardFooter}>
        <p>Other</p>
      </div>
    </div>
    );
  }
}

const me = {

  main : { backgroundColor: "#f5f6f8", height: '100%', textAlign: 'center',
          fontFamily: '-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol' },
  frame : { margin: 'auto', padding: '60px 0 0 0' ,width: '800px', textAlign: 'left' },
  head : { height: 'auto', padding: '20px 0', color: 'white' },
  about : { display: 'flex' },
  profile : { borderRadius: '200px', marginRight: '15px', marginTop: '10px', marginBottom: '20px', height: '130px' },
  title : { fontSize: 27, textTransform: 'uppercase', fontSize: '5em', 
            color: "#818ea3", marginBottom: '-1.3rem', marginTop: 0},
  name : { color: '#181d27', fontSize: '27px' },
  row : { display: 'flex', flexWrap: 'wrap', margin: 'auto', width: '900px'},
  card : { marginBottom: '1.5rem', backgroundColor: 'white', width: '20%', padding: '15px', borderRadius: '25px',
          flex: '1', marginLeft: '10px', marginRight: '10px', boxShadow: "5px 8px 10px #DEDEDE" },
  textCard : {letterSpacing: '.0625rem', marginBottom: '-0.7rem', color: '#818ea3', textTransform: 'uppercase' },
  group : { backgroundColor: '#fff', border: 'none', borderRadius: '.625rem', width: '800px', margin: 'auto', 
            marginBottom: '2rem', boxShadow: "5px 8px 10px #DEDEDE" },
  cardHead : { borderRadius: '.625rem .625rem 0 0', textAlign: 'left', padding: '0.4rem 1rem', fontSize: '35px' },
  tHead : { margin: '15px 0' },
  cardBody : {padding: '0'},
  listGroup : { display: '-webkit-box', display: '-ms-flexbox', display: 'flex', flexDirection: 'column', paddingLeft: 0,
                marginTop: 0,marginBottom: 0, listStyleType: "none" },
  listItem : { borderTop: 0, borderRight: 0, borderLeft: 0, borderRadius: 0, display: 'flex', padding: '.625rem 1rem',
              fontSize: '.8125rem', fontFamily: "Arial, Helvetica, sans-serif", borderTop: '1px solid rgba(0,0,0,.125)' },
  textLeft : { fontWeight: '600', },
  textRight : { color: "#818ea3", textAlign: 'right', marginLeft: 'auto' },
  cardFooter : { borderRadius: '0 0 .625rem .625rem', padding: '0.1rem 0', borderTop: '1px solid #e1e5eb' }

}

export default Sirasit;