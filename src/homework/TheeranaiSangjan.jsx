import React, { Component } from 'react';
import "../App.css";

export default class Jackplus extends Component {
    render() {
        const photoUrl = 'https://goo.gl/JuHhw5';
        return (
            <div style={styles.container} >
                <div style={styles.card}>
                    <CardSection>
                        <CardList cardInfo="THEERANAI SANGJAN" />
                        <CardList cardInfo="5931305029" />
                        <CardList cardInfo="Software Engineering" />
                    </CardSection>
                    <CardSection>
                        <img style={styles.imgSize} alt="Theeranai" src={photoUrl} />
                    </CardSection>
                </div>


            </div >
        );
    }
}




class CardSection extends React.Component {
    render() {
        return (
            <div style={styles.cardSectionStyle}>{this.props.children}</div>
        );
    }
}

class CardList extends React.Component {
    render() {
        const { cardInfo } = this.props;
        return (
            <div style={styles.cardList}>
                <h3>{cardInfo}</h3>
            </div>
        );
    }
}




const styles = {

    container: {
        display: 'flex',
        justifyContent: 'center',
    },
    imgSize: {
        height: 200,
        width: null,
        borderRadius: 2,
    },
    card: {
        border: '2px solid',
        display: 'flex',
        width: 450,
        borderRadius: 3,
        padding: 5,
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: '#CD5C5C'

    },
    cardSectionStyle: {
        border: '5px solid',
        borderColor: '#DAF7A6',
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: '#FFA07A'
    },
    cardList: {
        display: "flex",
        flexWrap: "wrap",
        alignItems: "center",
        justifyContent: "center",
    }
}

