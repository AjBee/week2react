import React, { Component } from 'react';

export default class ThirawatPrasert extends Component {

    render() {
        const { container, card, img, gg, bb } = styles;
        return (
            <div style={card}>
               <img src={require('./image/firstt.gif')} alt='FIRST' style={img}/>
                <div style={container}>
                    <h4 style={gg}>Thirawat Prasert</h4>
                    <h4 style={bb}>Software Engineering</h4>
                    <p style={bb}>5931305018</p>
                </div>
            </div>
        );
    }
}

const styles = {

    card: {
        boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)',
        transition: '1s',
        width: '20%',
        textAlign: 'center',
        margin: 'auto',
        marginTop: '20px',
        backgroundColor:'#FFFF66',
        borderRadius: '25px'
    },

    container: {
        padding: '2px 16px',
    },

    gg:{
        fontFamily: 'cursive',
        fontSize:25
    },

    bb:{
        fontFamily: 'cursive'
    },

    img: {
        marginTop: "25px",
        hight:100,
        width:110,
        borderRadius: '50px',
        marginRight: '10px'
    }
}