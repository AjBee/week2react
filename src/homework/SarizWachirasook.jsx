import React, { Component } from 'react';

class SarizWachirasook extends Component {
    render() {
        const imgLink = 'https://scontent.fbkk14-1.fna.fbcdn.net/v/t1.0-9/36495176_2139864206043129_3810102124352634880_n.jpg?_nc_cat=0&_nc_eui2=AeEtSwYgfCWAoBttnVkCRHhgI37V9_BZqS63J6yxVLeXKwcNJc-9tekEPo1MBn5UBzmO8S9oKNpfxXysqvfuRoSfFdKzjIkGPqiXsMb5aAjNYg&oh=47d54760b422d54e4f5fcaf909b5ae7a&oe=5C2E8C30';
        return (
            <div style={styles.container}>
                <div style={styles.cardContainer}>

                    <div style={styles.cardImage}>
                        <img style={styles.image} src={imgLink} alt="Sariz" />

                    </div>
                    <div style={{ display: "flex", flex: 3 }}>
                        <div style={styles.innerCardText}>
                            <div style={{ display: "flex" }}>
                                <p style={styles.textDetail}>
                                    Sariz Wachirasook<br />
                                    5931305060<br />
                                    Software Engineering<br />
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

const styles = {
    container: {
        display: "flex",
        margin: 10,
        alignItems: "center",
        justifyContent: "center"
    },
    cardContainer: {
        display: "flex",
        alignItems: "flex-start",
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        height: "200px",
        width: "400px",
        boxShadow: "1px 4px 10px 1px rgba(0, 0, 0, 0.3)",
        borderRadius: 20
    },
    cardImage: {
        display: "flex",
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    image: {
        height: "120px",
        marginLeft: 20,
        width: "120px",
        borderRadius: 60,
        boxShadow: "1px 4px 10px 1px rgba(0, 0, 0, 0.3)"
    },
    innerCardText: {
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "flex-start",
        justifyContent: "center",
        marginLeft: 20
    },
    textDetail: {
        fontSize: 20,
        display: "flex",
        textAlign: "left",
        fontWeight: "bold",
        color: "rgba(0,0,0,0.75)"
    }
}

export default SarizWachirasook;
