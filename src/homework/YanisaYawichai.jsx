import React, { Component, } from 'react';
import "../App.css";

export default class YanisaYawichai extends Component {

    render() {
      
        return (
            <div style={styles.container}>
                <img style={styles.imgSize}  alt='cartoon' src={require('./image/tak2.jpg')} />      
                <div style={styles.bg}>
                    <div>
                        <img style={styles.imgPng}
                            src={require('./image/tak22.png')}
                            alt='png' />
                    </div>
                    <div style={styles.text}>
                        <h1 style={{ padding: 20 }}>Yanisa Yawichai</h1>
                        <h1>5931305016</h1>
                    </div>
                    <img style={styles.gif}
                        src={('https://i.pinimg.com/originals/91/f9/d6/91f9d60b33e95f7a6021488ecc136f39.gif')}
                        alt='gif' />
                </div>
            </div>
        );
    }
}

const styles = {
    container: {
        color: 'red',
        flexDirection: 'row',
        display: 'flex',
        marginTop: 50,
        marginBottom: 50,
        justifyContent: 'center',
        borderTopRightRadius: 30,
      
       

    },
    text: {
        height: 170,
        backgroundColor: '#E6E6FA',
      
    },
    imgSize: {
        width: "350px",
        height: "520px",
        borderBottomLeftRadius: 30,
        borderTopLeftRadius: 30,
       
    },
    bg: {
        width: "350px",
        height: "520px",
        color: '#6699FF',
        fontFamily: 'monospace',
        backgroundColor: '#F0FFF0',
        borderTopRightRadius: 30,

    },
    imgPng: {
        marginTop: 10,
        paddingBottom: 10,
        width: 120,
        height: 100,
      
    },
    gif:{
        width: 350, 
        height: 210 ,
        borderBottomRightRadius: 20,
    }
   
}