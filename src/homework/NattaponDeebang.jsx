import React, { Component } from 'react';

class NattaponDeebang extends Component {
    render() {
        const imgLink = 'https://www.picz.in.th/images/2018/09/09/fbn5tq.png';
        return (
            <div style={NStyle.container}>
            <table style={NStyle.tablesty}>
                <tr>
                    <td rowspan="2" width="30%"> <img style={NStyle.imgsty} src={imgLink} alt="Nattapon" /> </td>
                    <td><h1 style={NStyle.name}>Nattapon  Deebang</h1></td>
                </tr>                
                <tr>
                    <td><h2 style={NStyle.sid}>5 9 3 1 3 0 5 0 1 9</h2></td>
                </tr>
               </table> 
            </div>
        );
    }
}

const NStyle = {
    container:{
        backgroundColor: 'rgb(50, 50, 50)',
        border:'8px solid rgb(80, 80, 80)',
        borderWidth: '2' ,
        margin: 'auto' , 
        padding: '10px' ,
        width: '550px' ,
    },name:{
        color: '#f0f0f0' ,
        fontFamily: 'Courier New'
    },sid:{
        color: '#f6f6f6' ,
        fontFamily: 'Courier New'
    },imgsty:{
        height: '180px' ,
        width: '180px' ,
        borderRadius: '1',
    },tablesty:{
        margin:'auto',
    }
}

export default NattaponDeebang

;