import React, { Component } from 'react';
// import logo from './logo.svg';
// import './App.css';

class Siwadon extends Component {
    render() {
        return (
            <div style={tong.border}>
                <div style={tong.background}>
                    <p style={tong.M}>
                        <h1 style={tong.ID1}>ID 5931305057</h1>

                        <p style={tong.Name}> Name Siwadon Boonpradub </p>

                        <p style={tong.Uni}> Mae Fah Luang University </p>

                        <p style={tong.School}>School of Information Technology</p>

                        <p style={tong.Major}>Major : Software Engineering</p>
                        <img src="https://i.ytimg.com/vi/wjZ_0J3N4jA/maxresdefault.jpg" alt="profile" style={tong.Pic1} />

                        <hr style={tong.Line} />
                        <img src="https://scontent.fbkk9-2.fna.fbcdn.net/v/t1.0-9/23519232_1542270582532066_4550770188951218954_n.jpg?_nc_cat=0&oh=0761e50452b07295228560bf329fd766&oe=5C22775B" alt="profile" style={tong.Pic} />
                    </p>
                </div>
            </div>
        );
    }
}
const tong = {
    background: { textAlign: 'center', background: 'linear-gradient(to bottom, #0033cc 0%, #00ccff 100%)', width: 'auto', height: '100%' },
    border: { border: '4px solid', color: 'grey', width: '400px', margin: 'auto' },
    ID1: { fontSize: '25px', background: '-webkit-linear-gradient(#eee,#333)', WebkitBackgroundClip: 'text', WebkitTextFillColor: 'transparent', margin: '0' },
    Name: { fontSize: '15px', color: '#C2E1EF' },
    Uni: { fontSize: '15px', color: '#C2E1EF' },
    School: { fontSize: '15px', color: '#C2E1EF' },
    Major: { fontSize: '15px', color: '#C2E1EF' },
    Pic: { width: '250px', marginTop: '30px' },
    Pic1: { width: '125px' },
    Line: { width: '45%' },
    M: { marginTop: '0', }
}
export default Siwadon;
