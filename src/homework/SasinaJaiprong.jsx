import React, { Component } from 'react';
// import './App.css';

class Sasina extends Component {
  render() {
    return (
      <div style={style.main}>
        <Header myname =" Sasina Jaiprong"
                name =" Eye"
                ages =" 20 years"
                school=" Kanchananukroh school"
                id=" 591305053"
                university=" Mae Fah Luang University"
                maxim =" Come on you can do it. "/>
      </div>
    );
  }
}

class Header extends React.Component{
  render() {

    const profile = 'https://scontent.fbkk9-2.fna.fbcdn.net/v/t1.0-9/40376899_1676231812485234_4884144505869041664_n.jpg?_nc_cat=0&oh=7e1f736f1dd491c36fb79c5bae60e0e9&oe=5C208591',
          school = 'https://image.flaticon.com/icons/svg/201/201576.svg',
          about = 'https://image.flaticon.com/icons/svg/201/201567.svg';

    return (
      <div style={style.center}>
      
        {/* Card 1 */}
        <div style={style.box1}>
            <div style={style.aboutMe}>
              <div style={style.conLeft}>
                <img src={profile} style={style.img} alt="profile"></img>
              </div>
              <div style={style.conRight}>
                <h1 style={style.title}>About me!</h1>
                <h1>Eye Sasi</h1>
              </div>
            </div>
        </div>

        <div style={style.allBox}>
          <div style={style.box2}>
            <div style={style.img}>
              <img src={about} style={style.img1} alt="school"></img>
            </div>
            <div style={style.textAligeLeft}>
              <h2>Name : {this.props.myname}</h2>
              <h2>NickName : {this.props.name}</h2>
              <h2>Age : {this.props.ages}</h2>
            </div>
          </div><br/>
          <div style={style.box3}>
            <div style={style.img}>
              <img src={school} style={style.img1} alt="school"></img>
            </div>
            <div style={style.textAligeLeft}>
              <h2>High school : {this.props.school}</h2>
              <h2>University : {this.props.university}</h2>
              <h2>ID : {this.props.id}</h2>
            </div>
          </div>
        </div>
       
       {/* Footer */}
            <div style={style.box4}>
                  <h1 style={style.matix}>Maxim of me:{this.props.maxim }</h1>
            </div><br/>
            <hr style={style.line}/>
      </div>
    );
  }
}

const style = {

  main : { textAlign: 'center', fontFamily: 'cursive' },
  header : { border: '10px dotted rgb(255, 253, 253)', background: "#000000",
            height: '150px', padding: '20px', fontSize: '2em', textAlign: 'center', color: "#FFFFFF" },
  center : { textAlign: 'center' },
  allBox : { borderRadius: '15px', display: 'flex', width: '900px', margin: 'auto', },
  box1 : { padding: '35px', backgroundClip: 'padding-box',
            textShadow: '5px 8px 5px #FFFFFF', margin: '10px',flex: '1', width: '900px', margin: 'auto', },
  box2 : { border: '10px dotted black', padding: '35px', background: "#48D1CC", backgroundClip: 'padding-box',
            textShadow: '5px 8px 5px #FFFFFF', width: '400px', margin: '10px',flex: '1', height: '270px',
            fontSize: '12px' },
  box3 : { border: '10px dotted black', padding: '35px', background: "#40E0D0", backgroundClip: 'padding-box',
            textShadow: '5px 8px 5px #FFFFFF', width: '400px', margin: '10px',flex: '1', height: '270px',
            fontSize: '12px' },
  box4 : { backgroundClip: 'padding-box', textShadow: '5px 8px 5px #FFFFFF', 
           margin: 'auto', },
  img : { width: '200px', textAlign: 'center', margin: 'auto', borderRadius: '60px' },
  img1 : { width: '120px', textAlign: 'center', margin: 'auto' },
  textAligeLeft : { textAlign: 'left' },
  line : { width: '150px' },
  matix : { margin: '25px 0 5px 0', },
  aboutMe : { display: 'flex', marginLeft: '60px', },
  conLeft : { marginLeft: '20', marginRight: '35px',},
  conRight : { textAlign: 'left', marginTop: '-20px', },
  title : { fontSize: '50px' }
}

export default Sasina;
