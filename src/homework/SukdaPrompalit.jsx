import React, { Component } from 'react';

class Sukda extends Component {
    render() {
        return (
            <div style={stlye.card}>
            <img style={stlye.img} src ={require('./image/img1.jpg')} alt="my profile image"/>
            <h1>Sukda Prompalit</h1>
            <p style={stlye.title}>5931305054</p>
            <p>Mae Fah Luang University</p>
            <p><a href="https://www.facebook.com/sukda13"><button style={stlye.button} >Contact</button></a></p>               
            </div>
        );
    }
}

const stlye = {
    card:{
        boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2)', 
        maxWidth: '300px', 
        margin: 'auto', 
        textAlign: 'center', 
        fontFamily: 'arial'
    },

    title: {
        color: 'grey',
        fontsize: '18px'
      },

      button: {
        border: 'none',
        outline:' 0',
        display: 'inline-block',
        padding:' 8px',
        color: 'white',
        backgroundColor: '#000',
        textAlign: 'center',
        cursor: 'pointer',
        width: '100%',
        fontSize: '18px'
      },
      
      a :{
        textDecoration: 'none',
        fontSize:' 22px',
        color: 'black'

      },

      img:{
        width: '100%'
      }
      

    }


export default Sukda;