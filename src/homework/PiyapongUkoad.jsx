import React, { Component } from 'react';

class PiyapongUkoad extends Component {

    constructor(props) {
        super(props)
        this.state = {
            Facebook: 'https://www.facebook.com/0223199893855931305036Joe'
        }
    }

    render() {
        return (
            <div style={styles.profileCard}>
                <table>
                    <tr>
                        <td>
                            <div>
                                <img src={require('./image/joey.jpg')} style={styles.imgPro} />
                            </div>
                        </td>
                        <td style={styles.columnColor}>
                            <h1>Hello world!!!</h1>
                            <h3 style={styles.textShadow}>I'm Joey</h3>
                            <h3 style={styles.textShadow}>I'm a Software Engineering student at MFU</h3>
                            <h3 style={styles.textShadow}>5931305036</h3>
                            <h5>Find me in Facebook</h5>
                            <a href={this.state.Facebook} target='blank' > Yeoj Joey</a>
                        </td>
                    </tr>
                </table>
            </div >
        );
    }
}

const styles = {
    profileCard: {
        margin: '50px',
        backgroundColor: '#D3D3D3',
        border: '1px solid grey',
        borderRadius: '25px',
        width: '600px',
        boxShadow: '4px 4px 8px 0 rgba(0, 0, 0, 0.5)',
        alignItems: 'center',
    },

    imgPro: {
        width: '300px',
        height: 'auto',
        borderRadius: '25px',
    },

    textShadow: {
        textShadow: '2px 1px grey'
    },

    columnColor: {
        backgroundColor: 'white',
        width: '275px',
        height: '400px',
        borderRadius: '25px',
    },
}

export default PiyapongUkoad;