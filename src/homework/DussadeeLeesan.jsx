import React, { Component } from "react";

class DussadeeLeesan extends Component {
    render() {
        return (
            <div style={paistyle.container}>
                <div style={paistyle.textbox1}>
                    <div style={paistyle.p}>P</div>
                </div>
                <div style={paistyle.textbox}>
                    <h1 style={paistyle.text1}>5931305020</h1>
                    <h2 style={paistyle.text2}>Dussadee Leesan</h2>
                    <h2 style={paistyle.text3}>Software Engineering</h2>
                </div>
            </div>
        );
    }
}

const paistyle = {
    container: {
        // border: '1px solid black',
        width: '300px',
        height: '150px',
        padding: '10px',
        margin: 'auto',
        marginTop: '30px',
        marginBottom: '30px',
        borderRadius: '10px',
        background: '#FABC02',
        boxShadow: '5px 5px 20px -10px rgba(0, 0, 0, 1)',
        // align: 'center',

    },

    p: {
        fontSize: '160px',
        marginTop: '-15px',
        marginLeft: '-10px',
        color: 'maroon'
    },

    textbox: {
        // border: '1px solid black',
        width: '145px',
        height: '150px',
        float: 'left',
        marginLeft: '5px',
        marginTop: '25px',
        // position: 'absolute',
        // padding: '60px',
    },

    textbox1: {
        // border: '1px solid black',
        width: '145px',
        height: '150px',
        float: 'left',
        // position: 'absolute',
        // padding: '60px',
    },

    text1: {
        fontSize: '1rem',
        textAlign: 'left',
        letterSpacing: '0.3rem',       
        color: '#8E6211',
    },

    text2: {
        fontSize: '1rem',
        textAlign: 'left',
        color: '#8E6211',
    },

    text3: {
        fontSize: '0.8rem',
        textAlign: 'left',
        color: '#8E6211',
    }
}


export default DussadeeLeesan;