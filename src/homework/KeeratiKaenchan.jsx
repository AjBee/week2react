import React, { Component } from "react";
export default class KeeratiKaenchan extends Component {

    render() {
        const { container, header,img } = styles;
        return (
            <div style={container}>
                <img style={img} src ={require('./image/keerati.jpg')} alt='my profile image'/>
                <h1 style={header}>Keerati Kaenchan</h1>
                <h1 style={header}> 5931305079</h1>
                <h1 style={header}> Software Engineering</h1>
            </div>
        );
    }
}
const styles = {
    container:{ 
        align: 'center',
        margin: 'auto',
        border: '20px solid lightGray', 
        width: '800px',
        background:'gray',
        borderRadius: '100px'
        
    },
    header:{ 
        color: "lightgray" 
},

    img:{
        marginTop: '20px',
        width: '25%',
        border:'10px solid lightgray',
        borderRadius: '45px'
        
    }
}
