import React from 'react';

export default class Sapa058 extends React.Component {
    render(){
        return(
            <div style={styles.contain}>
            <div style={styles.kn}>
                <br></br><h1 style={styles.id}>5931305058</h1><br></br>
                <h2 style={styles.name}>SAPAPORN<br></br>KRIANGKIRATIKUL</h2>
                <h3 style={styles.major}>SOFTWARE ENGINEERING</h3>
            </div>
            </div>
        );
    }
}

const styles = {
    kn:{
        //border: '1px solid black',
        width: 450,
        height: 3,
        padding: 10,
        margin: 'auto',
        marginTop: 30,
        marginBottom: 30,
        borderRadius: 10,
        background: '#000000',
        boxShadow: '5px 5px 20px -10px rgba(0, 0, 0, 1)',
        //align: 'center',
    },
    id:{
        fontSize: '1rem',
        textAlign: 'center',
        color: '#000000',
    },
    name:{
        fontSize: '1rem',
        textAlign: 'center',
        letterSpacing: '0.3rem',
        color: '#000000',
    },
    major:{
        fontSize: '3rem',
        textAlign: 'center',
        letterSpacing: '0.3rem',
        color: '#191919',
        fontWeight: 900,
        lineHight: 1,
    },
    contain:{
        //height: 500,
        //width: null,
        //backgroundColor: '#330000',
    }
}