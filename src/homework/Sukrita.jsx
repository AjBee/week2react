import React, { Component } from "react";

class Sukrita extends Component {
  render() {
    return (
      <div style={jangstyle.container}>
        <h1 style={jangstyle.name}>- - - - - - - - - - Sukrita Gayasit - - - - - - - - - -</h1>
        <h1 style={jangstyle.id}>- - - - - - - - 5931305063 - - - - - - - -</h1>
        <h2 style={jangstyle.se}>Software Engineering</h2>
      </div>
    );
  }
}

const jangstyle={
    container:{
        align: 'center',
        width: '800px' ,
        background: '#58646D', 
        margin: 'auto',
        padding: '50px',
    },

    name:{
        color:'#F4CABB'
    },

    se:{
        color:'white',
    },

    id:{
        color:'#E6C570' 
    }

}

  
export default Sukrita;
