import React, { Component } from "react";

class SuvichanKumtue extends Component {
    render() {
        return (
            <div style={artstyle.container}>
                <img src={require('./image/art.jpg')}
                    style={artstyle.image} img />
                <h1 style={artstyle.text}>Suvichan Kumtue</h1>
                <h1 style={artstyle.text}>5931305059</h1>
                <h4 style={artstyle.text}>Software Engineer</h4>
            </div>
        );
    }
}

const artstyle = {
    container: {
        width: '300px',
        height: 'auto',
        padding: '10px',
        margin: 'auto',
        marginTop: '30px',
        marginBottom: '30px',
        borderRadius: '10px',
        background: '#141F8E',
        boxShadow: '5px 5px 20px -10px rgba(0, 0, 0, 1)',
    },
    text: {
        color: "white",
        textAlign: 'right'
    },
    image:{
        width: '300px',
        height: 'auto',
    }

}


export default SuvichanKumtue;