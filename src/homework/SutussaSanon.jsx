import React, { Component } from "react";

export default class SutussaSanon extends Component {
  render() {
    return (
      <div>
        <div style={nubthong.container}>
          <img style={nubthong.img} src={require("./image/nt.png")} />
          <div style={nubthong.item}>
            <p>
              <h2>Sutussa Sanon</h2>
              <b>Software Engineering @ MFU</b>
              <br />
              5931305065
              <br />
            </p>
          </div>
        </div>
      </div>
    );
  }
}

const nubthong = {
  container: {
    margin: "1em 0",
    padding: "1em 0",
    display: "flex",
    flexWrap: "wrap",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    fontFamily:
      'BlinkMacSystemFont, -apple-system, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", "Helvetica", "Arial", sans-serif',
    backgroundColor: "#ddd",
    borderWidth: "2px 0",
    borderStyle: "solid",
    borderColor: "#888"
  },
  item: {
    padding: "0 2rem",
    margin: "0.5em 1ch",
    borderRadius: "5px",
    backgroundColor: "#eee"
  },
  img: {
    borderRadius: "100%",
    height: "15em",
    backgroundColor: "#eee"
  }
};
