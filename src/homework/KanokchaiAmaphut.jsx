import React, { Component } from "react";

class KanokchaiAmaphut extends Component {
  render() {
    return (
      <div style ={style.card}>
        <img src ={require('./image/tao.gif')}
        style = {style.img}img/>      
          <h1 style={style.h1}>5931305001</h1>
          <h2 style={style.h2}>Kanokchai Amaphut</h2>
          <p style={style.h2}>Software Engineering</p>

      </div>
    );
  }
}

const style={

    card: {
        boxShadow: '5px 5px 5px grey',
        transition: '1s',
        width: '30%',
        align: 'left',
        border: '4px solid gray',
        marginLeft:'10px',
        marginTop:'20px',
        marginBottom: '10px',

        
    },

    h1:{
        fontFamily: 'cursive',
        color:"#778899",
        fontSize:"35px",
        
    },

    h2:{
        fontFamily: 'cursive',
        color:"#708090",
        fontSize:"30px",
    },
    img:{
        border: '1px solid gray',
        marginTop: '10px',
        height:150,
        width:200
    }

    
}
export default KanokchaiAmaphut;
