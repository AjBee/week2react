import React, { Component } from "react";

export default class ThanapatSanongyat extends Component {
  render() {
    return (
      <div className="flukekie" style={flukekie.container}>
        <div style={flukekie.item}>
          <p>
            <b>Thanapat Sanongyat</b>
            <br />
            5931305025
          </p>
        </div>
        <div style={flukekie.item}>
          <p>
            <b>Software Engineering @ MFU</b>
            <br />
            Year 3 Student
          </p>
        </div>
        <div style={flukekie.item}>
          <p>
            <b>https://gitlab.com/flukekie</b>
            <br />
            <a href="mailto:gitlab@flukekie.net">gitlab@flukekie.net</a>
          </p>
        </div>
      </div>
    );
  }
}

const flukekie = {
  container: {
    padding: "1em 0 1em 0",
    display: "flex",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "center",
    fontFamily:
      'BlinkMacSystemFont, -apple-system, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", "Helvetica", "Arial", sans-serif'
  },
  item: {
    padding: "0 2rem 0 2rem",
    margin: "0.5em 1ch 0.5em 1ch",
    borderRadius: "5px",
    backgroundColor: "#eee"
  }
};
