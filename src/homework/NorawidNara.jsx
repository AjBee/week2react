import React, { Component } from 'react';

class NorawidNara extends Component {
    render() {
        return (
            <div style={bamestlye.container}>
                <div style={bamestlye.profile}>
                    <img src={require('./image/ImgKAITO.jpg')} style={bamestlye.pic} />
                    <div style={bamestlye.textContainer}>
                        <h1 style={bamestlye.name}>Norawid Nara</h1>
                        <div style={bamestlye.title}>5931305031</div>
                        <div style={bamestlye.title}>Software Engineering</div>
                    </div>
                </div>
            </div>
        );
    }
}

const bamestlye = {
    $profileW: '600px',
    $profileH: '$profileW / 5 * 2',
    $picSize: '$profileW',
    $textContainerWidth: '$profileW - $picSize',
    $textContainerMarginLeft: '$profileW - $textContainerWidth',

    container: { 
        background: 'linear-gradient(to right bottom, #430089, #82ffa1)', 
        border: '5px solid #AAA', 
        marginTop: '10px', 
        marginBotton: '10px', 
        padding: '5px',
        height: '280px' 
    },
    profile: {
        width: '$profileW',
        height: '$profileH',
        top: '0',
        right: '0',
        bottom: '0',
        left: '0',
        margin: 'auto',
        fontFamily: 'cursive',
        color: '#e0f7fa'
    },
    pic: {
        width: '200px',
        height: 'auto',
        float: 'left',
        marginLeft: '10px',
        borderRadius: '50% 0',
        boxShadow: '0 0 10px rgba(0,0,0,.3)'
    },
    textContainer: {
        width: '$textContainerWidth',
        marginLeft: '$textContainerMarginLeft',
        paddingTop: '$profileH / 5 * 2',
        textAlign: 'center',
        textShadow: '0 0 15px rgba(0,0,0,.1)'
    },
    name: {
        marginBottom: '$profileH / 7',
        fontWeight: '400',
        fontSize: '2.8em',
        lineHeight: '1em',
        letterSpacing: '-1px',
        textTransform: 'uppercase'
    },
    title: {
        fontSize: '1.4em',
        letterSpacing: '1px'
    }
}

export default NorawidNara;