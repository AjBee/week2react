import React, { Component } from 'react';

export default class PisitIntamo extends Component {

    constructor(props) {
        super(props)
        this.state = {
            imageUrl: [
                'https://i.kinja-img.com/gawker-media/image/upload/s--ZYYCjNJ---/c_fill,f_auto,fl_progressive,g_center,h_675,q_80,w_1200/n6vkkvnvlmp7di9gageo.gif',
                'https://i.redd.it/1jj71qtwaa711.jpg',
                'https://i.redd.it/qqa554z8gm711.jpg',
                'http://media.comicbook.com/2018/04/spider-man-1099203.jpeg',
                'http://gameaxis.com/wp-content/uploads/2018/09/spider-man-hero.jpg',
                'https://static.gamespot.com/uploads/screen_kubrick/1165/11653967/3393264-spider-man-ps4.jpg',
                'https://www.androidcentral.com/sites/androidcentral.com/files/styles/xlarge/public/article_images/2018/09/spider-man-ps4-promo-image.jpg?itok=s4NGKvS2',
                'https://gametimers.it/wp-content/uploads/2018/08/Marvel-Spider-Man.jpg',
                'http://images.pushsquare.com/72af4c011dc64/spider-man-faq-ps4-playstation-4-1.original.jpg',
                'https://media.playstation.com/is/image/SCEA/marvels-spider-man-screen-08-ps4-us-30mar18?$native_nt$',
                'https://cdn.vox-cdn.com/thumbor/aW9AtSWLyC0WVTCKMXUnKn48uq4=/0x0:1920x1090/1400x788/filters:focal(409x531:715x837):format(png)/cdn.vox-cdn.com/uploads/chorus_image/image/55230439/vlcsnap_2017_06_12_21h55m33s277.0.png',

            ],
            logo: 'https://pre00.deviantart.net/86c2/th/pre/f/2016/166/7/f/spider_man_ps4_symbol_by_yurtigo-da6d5oz.png',
            index: 0,
            facebookUrl: 'https://www.facebook.com/fuwa.raffey'
        }
    }
    //click next image
    goToNextSlide = () => {
        if (this.state.index === this.state.imageUrl.length - 1) {
            return this.setState({
                index: 0
            })
        }
        this.setState(prevState => ({
            index: prevState.index + 1
        }));
    }
    //click pre image
    goToPreSlide = () => {
        if (this.state.index === 0) {
            return this.setState({
                index: this.state.imageUrl.length - 1
            })
        }
        this.setState(prevState => ({
            index: prevState.index - 1
        }));
    }

    render() {
        return (
            <div style={styles.container}>
                <div style={styles.shadowCard}>
                    {/* image */}
                    <Slide image={this.state.imageUrl[this.state.index]} />
                    {/* button and index */}
                    <Button
                        goToPreSlide={this.goToPreSlide}
                        goToNextSlide={this.goToNextSlide}
                    >
                        <b>
                            {this.state.index + 1}
                            <b style={{ fontSize: 15 }}>
                                {' / ' + this.state.imageUrl.length}
                            </b>
                        </b>
                    </Button>
                    {/* name */}
                    <div style={styles.textCard}>
                        <b style={styles.textName}>P I S I T - I N T A M O</b>
                        <p>5 9 3 1 3 0 5 0 4 1</p>

                        <img
                            src={this.state.logo}
                            alt={'Spider man Logo'}
                            style={styles.logoStyle}
                        />
                    </div>
                    {/* footer */}
                    <div style={styles.footers} >
                        <a
                            href={this.state.facebookUrl}
                            target='blank'
                        >
                            <button style={styles.buttonContact}>CONTACT</button>
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}

// show image slide
const Slide = (props) => {
    return (
        <div>
            {/* header line */}
            <div style={styles.line} />
            <img
                src={props.image}
                alt={'Spider man'}
                style={styles.imgSlide} />

            {/* footer line */}
            <div style={styles.line} ></div>
        </div>
    )
}

// button 
const Button = (props) => {
    return (
        <div style={styles.buttonCard}>
            {/* Arrow left */}
            <b
                onClick={props.goToPreSlide}
                style={styles.buttons}
            >
                {'<'}
            </b>
            <div style={styles.textIndex} >

                {props.children}

            </div>
            {/* Arrow right */}
            <b
                onClick={props.goToNextSlide}
                style={styles.buttons}
            >
                {'>'}
            </b>
        </div>
    )
}

const colors = {
    primary: 'rgb(193,16,44)',
    secondary: 'rgb(11,97,191)',
    text: '#fff',
    lineColor: '#000'
}

const styles = {
    container: {
        display: 'flex',
        justifyContent: 'center',
        margin: 10,
        fontFamily: `"Palatino Linotype", "Book Antiqua", Palatino, serif`,
    },
    shadowCard: {
        boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2)'
    },
    imgSlide: {
        display: 'flex',
        height: 280,
        width: null,
        overflow: 'hidden'
    },
    line: {
        display: 'flex',
        height: 35,
        backgroundColor: colors.lineColor
    },
    buttons: {
        flex: 1,
        display: 'flex',
        border: 'none',
        fontWeight: 'bold',
        fontSize: 20,
        color: colors.text,
        alignItems: 'center',
        justifyContent: 'center',
        cursor: 'pointer'
    },
    textIndex: {
        flex: 3,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 30,
        color: colors.text
    },
    buttonCard: {
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: colors.secondary,
        height: 50,
        boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.3)'
    },
    textCard: {
        height: 180,
        backgroundColor: colors.primary,
        color: colors.text,
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
        padding: 10
    },
    textName: {
        fontSize: 20,
        marginTop: 10
    },
    logoStyle: {
        height: 100,
        width: null
    },
    footers: {
        display: 'flex',
        height: 50,
        backgroundColor: colors.secondary,
        alignItems: 'center',
        justifyContent: 'center',
        boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2)'
    },
    buttonContact: {
        fontSize: 16,
        fontWeight: 'bold',
        color: colors.text,
        border: 'none',
        backgroundColor: colors.secondary,
        fontFamily: `"Palatino Linotype", "Book Antiqua", Palatino, serif`,
        cursor: 'pointer'
    }
}
