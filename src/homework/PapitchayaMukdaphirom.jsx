import React, { Component } from "react";

class PapitchayaMukdaphirom extends Component {
  render() {
    return (
      <div style={bamstyle.container}>
        <h1 style={bamstyle.header}>Papitchaya Mukdaphirom</h1>
        <h1 style={bamstyle.h2}>ID 5931305034</h1>
        <img src={require('./image/bam3.jpg')}
        style={bamstyle.imagebam}
        />
      </div>
    );
  }
}

const bamstyle = {
  container: {
    border: '8px solid #FFFFFF',
    backgroundImage:`url(${require('./image/bam1.jpg')})`,
    marginTop: '10px',
    marginBottom: '10px',
    padding: '10px' ,
    margin: 'auto' ,
    width: '470px'
    
  },
  header: {
    color: "#FFFFFF"
  },
  h2: {
    color: "#FFFFFF",
    fontSize: "20px"
  },
  imagebam: {
    height: 110,
    width: 180,
    borderRadius: '120px',
   
  
  }



}


export default PapitchayaMukdaphirom;
