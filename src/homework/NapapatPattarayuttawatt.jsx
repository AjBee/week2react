import React, { Component } from 'react'

export default class NapaphatPattarayuttawatt extends Component {
  render() {
    return (
      <div>
        <div className="Nuukie" style={Nuukie.container}>
        <div style={Nuukie.item}>
        <img style={Nuukie.img} src={require("./image/Nuk.jpg")}/>
        <div style={Nuukie.item}>
          <p>
            <b>Napaphat Pattarayuttawatt</b>
            <br />
            ID: 5931305072
          </p>
        </div>
        <div style={Nuukie.item}>
          <p>
            <b>Major : Software Engineering </b>
          </p>
        </div>
        </div>
      </div>
      </div>
    )
  }
}


const Nuukie = {
  container: {
    padding: "1em 0 1em 0",
    display: "flex",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "center",
    fontFamily:'Monospace, Arial, Helvetica, sans-serif',
    fontSize: 25,
    color: 'white'
  },
  item: {
    padding: "0 2rem 0 2rem",
    margin: "0.5em 1ch 0.5em 1ch",
    borderRadius: "20px",
    backgroundColor: "#ff99ff",
    
  },
  img: {
    hight:200,
    width:280,
    border:'10px',
    borderRadius: '100px',
    border:'8px solid white'
  }
};
