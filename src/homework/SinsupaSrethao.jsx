import React, { Component } from "react";

class SinsupaSrethao extends Component {
  render() {
    const { fontw, card , img, des } = warnstyle;
    return (
        
        <div style={card}>
            <img style={img} src ={require('./image/sin.jpg')} alt='my profile image'/>
            <h1 style={fontw}>Sinsupa Srithao</h1>
            <h3 style={des}>5931305056</h3>
            <h3 style={des}>Software Engineering</h3>
      </div>
    );
  }
}

const warnstyle={
    card:{
        
        boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2)', 
        maxWidth: '300px', 
        margin: 'auto', 
        textAlign: 'center', 
        fontFamily: 'arial',
        marginTop: "25px"
    },

    des: {
        fontFamily:'monospace',
        fontsize: '20px'
      },
      
      fontw :{
        fontFamily:'monospace'

      },

      img:{
        width: '100%'
      }
}
  
export default SinsupaSrethao;
