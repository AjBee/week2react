import React, { Component } from 'react';

class KanokwanRuanthai extends React.Component {
    render() {
        const photo = 'https://scontent.fbkk5-3.fna.fbcdn.net/v/t1.0-9/40623770_2246129905437853_2144950029612220416_n.jpg?_nc_cat=0&oh=ba3a09ddcc46a93ede45ba5702a78a1c&oe=5C3AD9BA';
              
        return (
            <div style={styles.container}>
              <div style={styles.cardView}>
                   <img src={photo} alt='kanokwan' style ={styles.photoContainer}/>
              </div>
                   
                    


                <InfoCard

                    id="5931305002"
                    nameInfo="Kanokwan Ruanthai (Pang)"
                    major="Software Engineering"
                />


            </div>
        );
    }
}



class InfoCard extends Component {
    render() {
        const { id, nameInfo, photoChildren,major } = this.props;

        return (
            <div style={styles.cardView}>
                <h1>{id}</h1>
                <h2>{nameInfo}</h2>
                <h3>{major}</h3>
               

            </div>
        );
    }
}

const styles = {
    container: {
        border: '10px solid',
        borderColor: '#376D8E',
        borderRadius: 10,
        display: 'flex',
        flexDirection: 'column',
        
    },
    cardView: {
        borderWidth: 5,
        borderColor: 'blue',
        shadowOffset: 2,
        backgroundColor: '#89D3FF',
        

    }, 
    photoContainer: { 
        border: '10px solid',
        borderColor: '#376D8E',
        marginTop:50,
        borderRadius: 5,
        shadowOffset: 10,
        justifyContent: 'center',
        width:200,
        height: null,
    },
   
}


export default KanokwanRuanthai;

