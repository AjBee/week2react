import React, { Component } from "react";

class PennapaKhanthanan extends Component {
  render() {
    const send = 'https://image.flaticon.com/icons/svg/214/214316.svg';
    const contact = 'https://image.flaticon.com/icons/svg/552/552489.svg';
    // const stamp = 'https://image.flaticon.com/icons/svg/471/471219.svg';
    return (
      <div style={Joystyle.container}>
        <div style={Joystyle.leftbox}>
          <h1 style={Joystyle.headerL}>Sender <img style={Joystyle.icon} src={send} alt="sender"></img></h1>
          <h1 style={Joystyle.txtleft}>Name: Pennapa Khanthanan</h1>
          <h1 style={Joystyle.txtleft}>Nickname: Joy</h1>
          <h1 style={Joystyle.txtleft}>Homeland: NakhonSawan</h1>
          <h1 style={Joystyle.txtleft}>Student ID: 5931305042</h1>
          <h1 style={Joystyle.txtleft}></h1>
        </div>

        <div style={Joystyle.rightbox}><img style={Joystyle.image} src={require('./image/joy.jpg')} alt="pennapa"></img></div>
        {/* <div style={Joystyle.boxstamp}><img style={Joystyle.stamp} src={require('./image/joy2.png')} alt="stamp"></img></div> */}
        {/* <div style={Joystyle.boxstamp}><img style={Joystyle.stamp} src={stamp} alt="stamp"></img></div> */}
        <div style={Joystyle.buttombox}>
          <h1 style={Joystyle.headerR}>Contact <img style={Joystyle.icon} src={contact} alt="contactt"></img></h1>
          <h1 style={Joystyle.txtright}>Major: Software Engineering</h1>
          <h1 style={Joystyle.txtright}>Mae Fah Luang University</h1>
          <h1 style={Joystyle.txtright}>Email: 5931305042@lamduan.mfu.ac.th</h1>
        </div>


      </div>



    );
  }
}

const Joystyle = {
  container: {
    // background: 'linear-gradient(to top, #EB314A,#CC3277,#994690,#61518F,#375078,#2F4858)',
    // border:'5px solid #2F4858', 
    // background: 'linear-gradient(to top,#2F4858, #EB314A)',
    background: 'linear-gradient(to top,#C6FFDD, #FBD786,#f7797d)',
    padding: '1px',
    width: '700px',
    height: '500px',
    margin: 'auto',
    marginTop: '15px',
    marginBottom: '15px',
  },

  image: {
    borderRadius: "100%",
    border: '5px solid #A3E7D8 ',
    height: '200px',
    marginTop: '-30px',
  },

  rightbox: {
    margin: 'auto',
    marginRight: '1px',
    marginTop: '-200px',
    width: "330px",
    height: "100px",

  },

  leftbox: {
    background: '#52c234',
    // opacity: '0.5',
    borderRadius: "10%",
    marginLeft: '10px',
    marginTop: '10px',
    border: '5px solid #2F4858',
    width: "330px",
    height: "240px",


  },

  buttombox: {
    background: '#F15F79',
    borderRadius: "10%",
    border: '5px solid #2F4858',
    marginLeft: '350px',
    marginRight: '100px',
    marginTop: '100px',
    width: "330px",
    height: "210px",
  },

  txtleft: {
    fontFamily: 'segoe print',
    fontSize: '15px',
    fontWeight: 'bold',
    color: 'White',
    textAlign: 'Left',
    marginLeft: '15px',
    marginTop: '1px',


  },

  txtright: {
    fontFamily: 'segoe print',
    fontSize: '16px',
    fontWeight: 'bold',
    textAlign: 'Left',
    marginLeft: '15px',
    marginTop: '1px',

  },

  headerL: {
    fontFamily: 'segoe print',
    fontSize: '20px',
    fontWeight: 'bold',
    textAlign: 'Left',
    marginLeft: '15px',
    marginTop: '1px',
    color: 'White',
  },

  headerR: {
    fontFamily: 'segoe print',
    fontSize: '20px',
    fontWeight: 'bold',
    textAlign: 'Left',
    marginLeft: '15px',
    marginTop: '1px',

  },
  icon: {
    height: 50,
    width: 50,
    // marginBottom:'100px',
  },

  // stamp: {
  //   width: "70px",
  //   height: "70px",
  // },

  // boxstamp: {
  //   border: '5px solid #2F4858',
  //   margin: 'auto',
  //   marginRight: '1px',
  //   marginTop: '-200px',
  //   width: "330px",
  //   height: "100px",
  // },
}



export default PennapaKhanthanan;
