import React, { Component } from 'react'

class Yotsaphon extends Component {
    render() {
        return (
            <div style={Yotstyle.container}>
                <h1 style={Yotstyle.colortext}>59313050044</h1>
                <h2 style={Yotstyle.colortext}>Yotsaphon Chueamueangphan</h2>
            </div>
        );
    }
}
const Yotstyle = {
    container: {
        border: '1rem solid #2E4849',
        backgroundColor: "#0A2A53",
        marginTop: 40,

    },

    colortext: {
        color: "#FFB666",
        fontSize: 40
    },
}


export default Yotsaphon;
