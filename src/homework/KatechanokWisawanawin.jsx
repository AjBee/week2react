import React, { Component } from 'react'

export default class KatechanokWisawanawin extends Component {
  render() {
    return (        
      <div className="block" style={block.container}>
        <div style={block.item}>
        <img src={require("./image/kate.png")}/>
          <p>
            <b>Katechanok Wisawanawin</b>
            </p>
            <div style={block.item}>
            <p>
            <b>Software Engineering</b>
            <br />
            ID : 5931305070
          </p>
          </div>
        </div>
      </div>
    );
  }
}




const block = {
  container: {
    padding: "1em 0 1em 0",
    display: "flex",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "center",
    },
  item: {
    padding: "0 2rem 0 2rem",
    margin: "0.5em 1ch 0.5em 1ch",
    borderRadius: "5px",
    backgroundColor: "#F0FFFD"
  }
};
