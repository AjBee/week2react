import React, { Component } from 'react'

export default class SaranSrisathon extends Component {
  render() {
    return (
      <div>

        <div className="michael" style={michael.container}>
        <div style={michael.item}>
          <p>
            <b>Saran Srisathon</b>
            <br />
            Software Engineering
            <br />
            Mae Fah Luang University
            <br />
            5 7 3 1 3 0 5 1 1 0
          </p>
        </div>
        

      </div>

      </div>
    )
  }
}

const michael = {
    container: {
      backgroundColor: 'rgb(50, 50, 50)',
      border:'8px solid rgb(80, 80, 80)',
      padding: "1em 0 1em 0",
      display: "flex",
      flexWrap: "wrap",
      alignItems: "center",
      justifyContent: "center",
      fontFamily:
        'BlinkMacSystemFont, -apple-system, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", "Helvetica", "Arial", sans-serif'

    },
    item: {
      padding: "0 10rem 0 10rem",
      margin: "0.5em 1ch 0.5em 1ch",
      borderRadius: "50px",
      backgroundColor: "#CBCBCB"
    }
  };